<?php 

require('ape.php');
require('frog.php');

echo " <h1>Release 0</h1> ";    
$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo " Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded; // "no"

echo " <h1>Release 1</h1> ";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>";
echo " Legs : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
$sungokong->yell(); // "Auooo"

echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo " Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
$kodok->jump(); // "hop hop"
?>